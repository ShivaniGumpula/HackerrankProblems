import System.IO

helloWorlds n =
    sequence_ [putStrLn "Hello World"|i <-[1..n]]
    
main = do
    n <- readLn :: IO Int
    helloWorlds n



