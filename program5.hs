object FilterArray extends App{
def f(delim:Int,arr:List[Int]):List[Int] =

{ val l = scala.collection.mutable.ListBuffer.empty[Int]

arr.foreach { i => {if (i < delim) l+=i } }

l.toList

}

println(f(8,List(8,2,3,4,6,7,8,9)))

}
